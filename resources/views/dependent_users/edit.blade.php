@extends('layouts.app')

@section('content')
    <section class="content-header" style="padding-left: 226px">
        <div class="container-fluid my-2">
            <div class="row mb-2" style="margin: 0">
                <div class="col-sm-6">
                    <h1>Edit Dependent User</h1>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('dependent_users.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <form action="{{ route('dependent_users.update', $dependentUser->id) }}" method="POST" name="dependentUserForm" id="dependentUserForm">
            @csrf
            @method('PUT')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Tên nhân viên</label>
                                <select name="user_id" id="user_id" class="form-control">
                                    <option value="">Chọn nhân viên</option>
                                    @if ($users->isNotEmpty())
                                        @foreach ($users as $user)
                                            <option {{ $dependentUser->user->full_name ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->full_name }}, ({{ $user->id }})</option>
                                        @endforeach
                                    @endif
                                </select>
                                <p class="error"></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Tên người phụ thuộc</label>
                                <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Title"
                                    value="{{ $dependentUser->full_name }}">
                                <p class="error"></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Tuổi</label>
                                <input type="number" name="age" id="age" class="form-control"
                                    placeholder="Title" value="{{ $dependentUser->age }}">
                                <p class="error"></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Mối quan hệ</label>
                                    <input type="text" name="relationship" id="relationship" class="form-control"
                                        placeholder="Title" value="{{ $dependentUser->relationship }}">
                                    <p class="error"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pb-5 pt-3">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{ route('dependent_users.index') }}" class="btn btn-outline-dark ml-3">Cancel</a>
                </div>
            </div>
        </form>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection

@section('customJS')
    <script>
        $("#dependentUserForm").submit(function(event) {
            event.preventDefault();
            var element = $(this);
            $("button[type=submit]").prop('disabled', true);

            $.ajax({
                url: '{{ route('dependent_users.update', $dependentUser->id) }}',
                type: 'put',
                data: element.serializeArray(),
                dataType: 'json',
                success: function(response) {
                    $("button[type=submit]").prop('disabled', false);


                    if (response["status"] == true) {
                        window.location.href = "{{ route('dependent_users.index') }}";
                        $("#full_name").removeClass('is-invalid').siblings('p').removeClass(
                            'invalid-feedback').html("");
                        $("#age").removeClass('is-invalid').siblings('p').removeClass(
                            'invalid-feedback').html("");
                        $("#relationship").removeClass('is-invalid').siblings('p').removeClass(
                            'invalid-feedback').html("");
                    } else {
                        if (response['notFound'] == true) {
                            window.location.href = "{{ route('dependent_users.index') }}";
                        }

                        var errors = response['errors'];

                        if (errors['full_name']) {
                            $("#full_name").addClass('is-invalid').siblings('p').addClass('invalid-feedback')
                                .html(errors['full_name']);
                        } else {
                            $("#full_name").removeClass('is-invalid').siblings('p').removeClass(
                                'invalid-feedback').html("");
                        }

                        if (errors['age']) {
                            $("#age").addClass('is-invalid').siblings('p').addClass('invalid-feedback')
                                .html(errors['age']);
                        } else {
                            $("#age").removeClass('is-invalid').siblings('p').removeClass(
                                'invalid-feedback').html("");
                        }

                        if (errors['relationship']) {
                            $("#relationship'])").addClass('is-invalid').siblings('p').addClass('invalid-feedback')
                                .html(errors['relationship']);
                        } else {
                            $("#relationship'])").removeClass('is-invalid').siblings('p').removeClass(
                                'invalid-feedback').html("");
                        }
                    }
                },
                error: function(jqXHR, exception) {
                    console.log("Something went wrong");
                }
            })
        });
    </script>
@endsection