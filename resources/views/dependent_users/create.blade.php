@extends('layouts.app')

@section('content')
    <section class="content-header" style="padding-left: 226px">
        <div class="container-fluid my-2">
            <div class="row mb-2" style="margin: 0">
                <div class="col-sm-6">
                    <h1>Create Dependent User</h1>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('dependent_users.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <form action="{{ route('dependent_users.store')}}" method="POST" name="dependentUserForm" id="dependentUserForm">
            @csrf
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Tên nhân viên</label>
                                <select name="user_id" id="user_id" class="form-control">
                                    <option value="">Chọn nhân viên</option>
                                    @if ($users->isNotEmpty())
                                        @foreach ($users as $user)
                                            <option {{ $dependentUser->contains('user.full_name', $user->full_name) ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->full_name }}, ({{ $user->id }})</option>
                                        @endforeach
                                    @endif
                                </select>
                                <p class="error"></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Tên người phụ thuộc</label>
                                <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Title"
                                    >
                                <p class="error"></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Tuổi</label>
                                <input type="number" name="age" id="age" class="form-control"
                                    placeholder="Title">
                                <p class="error"></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Mối quan hệ</label>
                                    <input type="text" name="relationship" id="relationship" class="form-control"
                                        placeholder="Title">
                                    <p class="error"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pb-5 pt-3">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a href="{{ route('dependent_users.index') }}" class="btn btn-outline-dark ml-3">Cancel</a>
                </div>
            </div>
        </form>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection
