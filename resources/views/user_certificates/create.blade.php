@extends('layouts.app')

@section('content')
    <section class="content-header" style="padding-left: 226px">
        <div class="container-fluid my-2">
            <div class="row mb-2" style="margin: 0">
                <div class="col-sm-6">
                    <h1>Create User Certificate</h1>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('user_certificates.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <form action="{{ route('user_certificates.store')}}" method="POST" name="userCertificateForm" id="userCertificateForm">
            @csrf
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Tên nhân viên</label>
                                <select name="user_id" id="user_id" class="form-control">
                                    <option value="">Chọn nhân viên</option>
                                    @if ($users->isNotEmpty())
                                        @foreach ($users as $user)
                                            <option {{ $userCertificate->contains('user.full_name', $user->full_name) ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->full_name }}, ({{ $user->id }})</option>
                                        @endforeach
                                    @endif
                                </select>
                                <p class="error"></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Tên hồ sơ</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Title">
                                <p class="error"></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Ngày có hiệu lực</label>
                                <input type="date" name="start_at" id="start_at" class="form-control"
                                    placeholder="Title">
                                <p class="error"></p>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Ngày hết hiệu lực</label>
                                    <input type="date" name="expiry_at" id="expiry_at" class="form-control"
                                        placeholder="Title">
                                    <p class="error"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pb-5 pt-3">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a href="{{ route('user_certificates.index') }}" class="btn btn-outline-dark ml-3">Cancel</a>
                </div>
            </div>
        </form>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection
