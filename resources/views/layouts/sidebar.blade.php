<div class="sidebar pe-4 pb-3">
    <nav class="navbar bg-light navbar-light">
        <a href="index.html" class="navbar-brand mx-4 mb-3">
            <h3 class="text-primary"><i class="fa fa-hashtag me-2"></i>INISOFT</h3>
        </a>
        <div class="d-flex align-items-center ms-4 mb-4">
            <div class="position-relative">
                <img class="rounded-circle" src="{{ asset('assets/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
                <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
            </div>
            <div class="ms-3">
                <h6 class="mb-0">Jhon Doe</h6>
                <span>Admin</span>
            </div>
        </div>
        <div class="navbar-nav w-100">
            <ul>
                <li>
                    <a href="/" class="nav-item nav-link active">
                        <i class="fa fa-tachometer-alt me-2"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="{{ route('users.index') }}" class="nav-item nav-link active">
                        <i class="fas fa-users"></i>
                        <p>Nhân Viên</p>
                    </a>
                </li>
                <li>
                    <a href="{{ route('dependent_users.index') }}" class="nav-item nav-link active">
                        <i class="fas fa-address-book"></i>
                        <p>Người Phụ Thuộc</p>
                    </a>
                </li>
                <li>
                    <a href="{{ route('user_certificates.index') }}" class="nav-item nav-link active">
                        <i class="fas fa-address-card"></i>
                        <p>Hồ Sơ Nhân Viên</p>
                    </a>
                </li>
                <li>
                    <a href="{{ route('work_logs.index') }}" class="nav-item nav-link active">
                        <i class="fas fa-calendar-alt"></i>
                        <p>Bảng lương</p>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>