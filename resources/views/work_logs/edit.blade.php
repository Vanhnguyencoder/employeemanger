@extends('layouts.app')

@section('content')
<section class="content-header" style="padding-left: 226px">
    <div class="container-fluid my-2">
        <div class="row mb-2" style="margin: 0">
            <div class="col-sm-6">
                <h1>Edit Work Log</h1>
            </div>
            <div class="col-sm-6 text-right">
                <a href="{{ route('work_logs.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <form action="{{ route('work_logs.update', $workLog->id) }}" method="post" name="workLogForm" id="workLogForm">
        @csrf
        @method('PUT')
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Tên nhân viên</label>
                            <select name="user_name" id="user_name" class="form-control">
                                <option value="">Chọn nhân viên</option>
                                @if ($users->isNotEmpty())
                                    @foreach ($users as $user)
                                        <option {{ $workLog->user->full_name ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->full_name }}, ({{ $user->id }})</option>
                                    @endforeach
                                @endif
                            </select>
                            <p class="error"></p>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Số ngày làm việc</label>
                                <input type="number" name="title" id="title" class="form-control"
                                    placeholder="Title" value="{{ $workLog->work_date }}">
                                <p class="error"></p>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Thưởng</label>
                                <input type="number" name="title" id="title" class="form-control"
                                    placeholder="Title" value="{{ $workLog->bonus }}">
                                <p class="error"></p>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Phạt</label>
                                <input type="number" name="title" id="title" class="form-control"
                                    placeholder="Title" value="{{ $workLog->penalty }}">
                                <p class="error"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pb-5 pt-3">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('work_logs.index') }}" class="btn btn-outline-dark ml-3">Cancel</a>
            </div>
        </div>
    </form>
    <!-- /.card -->
</section>
<!-- /.content -->
@endsection