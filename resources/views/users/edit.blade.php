@extends('layouts.app')

@section('content')
<section class="content-header" style="padding-left: 226px">
    <div class="container-fluid my-2">
        <div class="row mb-2" style="margin: 0">
            <div class="col-sm-6">
                <h1>Edit User</h1>
            </div>
            <div class="col-sm-6 text-right">
                <a href="{{ route('users.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <form action="{{ route('users.update', $user->id) }}" method="POST" name="userForm" id="userForm">
        @csrf
        @method('PUT')
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Tên nhân viên</label>
                                <input type="text" name="user_name" id="user_name" class="form-control"
                                    placeholder="Title" value="{{ $user->full_name }}">
                                <p class="error"></p>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Ngày sinh</label>
                                <input type="date" name="birthday" id="birthday" class="form-control"
                                    placeholder="Title" value="{{ $user->birthday }}">
                                <p class="error"></p>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Giới tính</label>
                            <select name="gender" id="gender" class="form-control">
                                <option {{ $user->status == 0 ? 'selected' : '' }} value="0">Nữ
                                </option>
                                <option {{ $user->status == 1 ? 'selected' : '' }} value="1">Nam
                                </option>
                            </select>
                                <p class="error"></p>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Địa chỉ</label>
                                <input type="text" name="address" id="address" class="form-control"
                                    placeholder="Title" value="{{ $user->address }}">
                                <p class="error"></p>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Điện thoại</label>
                                <input type="text" name="mobile" id="mobile" class="form-control"
                                    placeholder="Title" value="{{ $user->mobile }}">
                                <p class="error"></p>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Mã số thuế</label>
                                <input type="number" name="tax_code" id="tax_code" class="form-control"
                                    placeholder="Title" value="{{ $user->tax_code }}">
                                <p class="error"></p>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <label for="title">Lương cơ bản</label>
                            <input type="number" name="salary" id="salary" class="form-control"
                                placeholder="Title" value="{{ $user->salary }}">
                            <p class="error"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pb-5 pt-3">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('users.index') }}" class="btn btn-outline-dark ml-3">Cancel</a>
            </div>
        </div>
    </form>
    <!-- /.card -->
</section>
<!-- /.content -->
@endsection