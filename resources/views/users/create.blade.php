@extends('layouts.app')

@section('content')
    <section class="content-header" style="padding-left: 226px">
        <div class="container-fluid my-2">
            <div class="row mb-2" style="margin: 0">
                <div class="col-sm-6">
                    <h1>Create Dependent User</h1>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('users.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <form action="{{ route('users.store')}}" method="POST" name="dependentUserForm" id="dependentUserForm">
            @csrf
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Tên nhân viên</label>
                                <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Title">
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Ngày sinh</label>
                                <input type="date" name="birthday" id="birthday" class="form-control" placeholder="Title">
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Giới tính</label>
                                <select name="gender" id="gender" class="form-control">
                                    <option value="0"> Nữ </option>
                                    <option value="1"> Nam </option>
                                </select>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Địa chỉ</label>
                                <input type="text" name="address" id="address" class="form-control" placeholder="Title">
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Điện thoại</label>
                                <input type="text" name="mobile" id="mobile" class="form-control" placeholder="Title">
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Mã số thuế</label>
                                <input type="number" name="tax_code" id="tax_code" class="form-control" placeholder="Title">
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <label for="title">Lương cơ bản</label>
                                <input type="number" name="salary" id="salary" class="form-control" placeholder="Title">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pb-5 pt-3">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a href="{{ route('users.index') }}" class="btn btn-outline-dark ml-3">Cancel</a>
                </div>
            </div>
        </form>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection
