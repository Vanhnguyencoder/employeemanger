<?php

namespace App\Repositories;

interface DependentUserRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Find dependent users by the ID of the primary user they depend on.
     *
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByUserId($userId);
}
