<?php

namespace App\Repositories;

use App\Models\UserCertificate;

class UserCertificateRepository extends BaseRepository implements UserCertificateRepositoryInterface
{
    /**
     * UserCertificateRepository constructor.
     *
     * @param UserCertificate $userCertificate
     */
    public function __construct(UserCertificate $userCertificate)
    {
        parent::__construct($userCertificate);
    }

    /**
     * Find user certificates by user ID.
     *
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByUserId($userId)
    {
        return $this->model->where('user_id', $userId)->get();
    }
}
