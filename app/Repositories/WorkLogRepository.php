<?php
namespace App\Repositories;

use App\Models\WorkLog;

class WorkLogRepository extends BaseRepository implements WorkLogRepositoryInterface
{
    /**
     * WorkLogRepository constructor.
     *
     * @param UserCertificate $userCertificate
     */
    public function __construct(WorkLog $workLog)
    {
        parent::__construct($workLog);
    }

    /**
     * Find work logs by user ID.
     *
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByUserId($userId)
    {
        return $this->model->where('user_id', $userId)->get();
    }

    public function list()
    {
        return $this->model->with('user')->get();
    }
}