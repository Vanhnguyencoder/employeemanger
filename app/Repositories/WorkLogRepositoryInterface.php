<?php

namespace App\Repositories;

interface WorkLogRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Find work log by user ID.
     *
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByUserId($userId);
}
