<?php

namespace App\Repositories;

interface BaseRepositoryInterface
{
    /**
     * Get all records.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Find a record by its primary key.
     *
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function find($id);

    /**
     * Create a new record.
     *
     * @param  array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data);

    /**
     * Update a record by its primary key.
     *
     * @param  int   $id
     * @param  array $data
     * @return bool
     */
    public function update($id, array $data);

    /**
     * Delete a record by its primary key.
     *
     * @param  int $id
     * @return bool|null
     */
    public function delete($id);
}
