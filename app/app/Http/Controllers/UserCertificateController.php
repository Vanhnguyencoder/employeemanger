<?php
namespace App\Http\Controllers;

use App\Models\UserCertificate;
use App\Repositories\UserCertificateRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserCertificateController extends Controller
{
    protected $userCertificateRepository;
    protected $userRepository;

    public function __construct(UserCertificateRepositoryInterface $userCertificateRepository, UserRepositoryInterface $userRepository)
    {
        $this->userCertificateRepository = $userCertificateRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $userCertificates = $this->userCertificateRepository->all();
        return view('user_certificates.index', compact('userCertificates'));
    }

    public function create()
    {
        // return view('certificates.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required|exists:users,id',
            'name' => 'required',
            'start_at' => 'required',
            'expiry_at' => 'required',
        ]);

        $userCertificate = $this->userCertificateRepository->create($validatedData);

        return response()->json([
            'status' => true,
            'message' => 'User certificate created successfully',
            'user_certificate' => $userCertificate
        ]);
    }

    public function show($id)
    {
        $userCertificate = $this->userCertificateRepository->find($id);
        return $userCertificate;
    }

    public function edit($id)
    {
        $userCertificate = $this->userCertificateRepository->find($id);
        $users = $this->userRepository->all();
        return view('user_certificates.edit', compact('userCertificate', 'users'));;
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'user_id' => 'required|exists:users,id',
            'name' => 'required',
            'start_at' => 'required',
            'expiry_at' => 'required',
        ]);

        $this->userCertificateRepository->update($id, $validatedData);

        return response()->json([
            'status' => true,
            'message' => 'User certificate updated successfully',
            'user_certificate' => [
                'user_id' => $request->user_id,
                'name' => $request->name,
                'start_at' => $request->start_at,
                'expiry_at' => $request->expiry_at
            ]
        ]);
    }

    public function destroy($id)
    {
        $this->userCertificateRepository->delete($id);
        return response()->json([
            'status' => true,
            'message' => 'User certificate deleted successfully',
        ]);
    }
}
