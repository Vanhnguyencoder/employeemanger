<?php
namespace App\Http\Controllers;

use App\Models\WorkLog;
use App\Repositories\WorkLogRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class WorkLogController extends Controller
{
    protected $workLogRepository;
    protected $userRepository;

    public function __construct(WorkLogRepositoryInterface $workLogRepository, UserRepositoryInterface $userRepository)
    {
        $this->workLogRepository = $workLogRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $workLogs = $this->workLogRepository->all();
        return view('work_logs.index', compact('workLogs'));
    }

    public function create()
    {
        return view('work_logs.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate();

        $workLog = $this->workLogRepository->create($validatedData);

        return response()->json([
            'status' => true,
            'message' => 'Work log created successfully',
            'work_log' => $workLog
        ]);
    }

    public function show($id)
    {
        $workLog = $this->workLogRepository->find($id);
        return $workLog;
    }

    public function edit($id)
    {
        $workLog = $this->workLogRepository->find($id);
        $users = $this->userRepository->all();
        return view('work_logs.edit', compact('workLog', 'users'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'user_id' => 'required|exists:users,id',
            'work_date' => 'required',
            'bonus' => 'required',
            'penalty' => 'required',
        ]);

        $this->workLogRepository->update($id, $validatedData);

        return response()->json([
            'status' => true,
            'message' => 'Work log update successfully',
            'work_log' => [
                'user_id' => $request->user_id,
                'work_date' => $request->work_date,
                'bonus' => $request->bonus,
                'penalty' => $request->penalty
            ]
        ]);
    }

    public function destroy($id)
    {
        $this->workLogRepository->delete($id);
        return response()->json([
            'status' => true,
            'message' => 'Work log deleted successfully',
        ]);
    }
}
