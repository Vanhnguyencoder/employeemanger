<?php
namespace App\Http\Controllers;

use App\Http\Requests\DependentUserRequest;
use App\Repositories\DependentUserRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class DependentUserController extends Controller
{
    protected $dependentUserRepository;
    protected $userRepository;

    public function __construct(DependentUserRepositoryInterface $dependentUserRepository, UserRepositoryInterface $userRepository)
    {
        $this->dependentUserRepository = $dependentUserRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $dependentUsers = $this->dependentUserRepository->all();
        return view('dependent_users.index', compact('dependentUsers'));
    }

    public function create()
    {
        // return view('dependent_users.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required|exists:users,id',
            'full_name' => 'required',
            'age' => 'required',
            'relationship' => 'required',
        ]);

        $dependentUser = $this->dependentUserRepository->create($validatedData);

        return response()->json([
            'status' => true,
            'message' => 'Dependent user created successfully',
            'dependent_user' => $dependentUser
        ]);
    }

    public function show($id)
    {
        $dependentUser = $this->dependentUserRepository->find($id);
        return $dependentUser;
    }

    public function edit($id)
{
    $dependentUser = $this->dependentUserRepository->find($id);
    $users = $this->userRepository->all();
    return view('dependent_users.edit', compact('dependentUser', 'users'));
}


    public function update(DependentUserRequest $request, $id)
    {
    
        return response()->json([
            'status' => true,
            'message' => 'Dependent user updated successfully',
            'dependent_user' => [
                'user_id' => $request->user_id,
                'full_name' => $request->full_name,
                'age' => $request->age,
                'relationship' => $request->relationship

            ]
        ]);
    }

    public function destroy($id)
    {
        $this->dependentUserRepository->delete($id);
        return response()->json([
            'status' => true,
            'message' => 'Dependent user deleted successfully',
        ]);
    }
}
