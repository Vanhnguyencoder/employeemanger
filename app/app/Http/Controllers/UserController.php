<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $users = $this->userRepository->all();
        return view('users.index', compact('users'));
    }

    public function create()
    {
        // return view('users.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'birthday' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'tax_code' => 'required',
            'salary' => 'required',
        ]);

        $user = $this->userRepository->create($validatedData);

        return response()->json([
            'status' => true,
            'message' => 'User created successfully',
            'user' => $user
        ]);
    }

    public function show($id)
    {
        $user = $this->userRepository->find($id);
        return $user;
    }

    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        return view('users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'birthday' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'tax_code' => 'required',
            'salary' => 'required',
        ]);

        $user = $this->userRepository->update($id, $validatedData);

        return response()->json([
            'status' => true,
            'message' => 'User updated successfully',
            'user' => [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'birthday' => $request->birthday,
                'gender' => $request->gender,
                'address' => $request->address,
                'mobile' => $request->mobile,
                'tax_code' => $request->tax_code,
                'salary' => $request->salary,
            ]
        ]);
    }

    public function destroy($id)
    {
        $user = $this->userRepository->delete($id);
        return response()->json([
            'status' => true,
            'message' => 'User deleted successfully',
        ]);
    }
}
