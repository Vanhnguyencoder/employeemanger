<?php

namespace App\Repositories;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Find a user by their id address.
     *
     * @param string $id
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function findById($id);
}
