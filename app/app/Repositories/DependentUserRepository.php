<?php

namespace App\Repositories;

use App\Models\DependentUser;

class DependentUserRepository extends BaseRepository implements DependentUserRepositoryInterface
{
    /**
     * DependentUserRepository constructor.
     *
     * @param DependentUser $dependentUser
     */
    public function __construct(DependentUser $dependentUser)
    {
        parent::__construct($dependentUser);
    }

    /**
     * Find dependent users by the ID of the primary user they depend on.
     *
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByUserId($userId)
    {
        return $this->model->where('user_id', $userId)->get();
    }
}
