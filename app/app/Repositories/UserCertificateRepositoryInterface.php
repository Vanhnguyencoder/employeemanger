<?php

namespace App\Repositories;

interface UserCertificateRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Find user certificates by user ID.
     *
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByUserId($userId);
}
