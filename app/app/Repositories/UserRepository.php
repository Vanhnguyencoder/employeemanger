<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct($user);
    }

    /**
     * Find a user by their id address.
     *
     * @param string $id
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function findById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function orderBy()
    {
        return $this->model->orderBy('first_name', 'ASC');
    }
}
