<?php

namespace App\Providers;

use App\Repositories\DependentUserRepository;
use App\Repositories\DependentUserRepositoryInterface;
use App\Repositories\UserCertificateRepository;
use App\Repositories\UserCertificateRepositoryInterface;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\WorkLogRepository;
use App\Repositories\WorkLogRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(
            UserRepositoryInterface::class,
            UserRepository::class,
        );
        
        $this->app->singleton(
            UserCertificateRepositoryInterface::class,
            UserCertificateRepository::class,
        );

        $this->app->singleton(
            DependentUserRepositoryInterface::class,
            DependentUserRepository::class,
        );

        $this->app->singleton(
            WorkLogRepositoryInterface::class,
            WorkLogRepository::class,
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
