<?php
namespace App\Http\Controllers;

use App\Http\Requests\UserCertificateRequest;
use App\Http\Requests\UserRequest;
use App\Models\UserCertificate;
use App\Repositories\UserCertificateRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserCertificateController extends Controller
{
    protected $userCertificateRepository;
    protected $userRepository;

    public function __construct(UserCertificateRepositoryInterface $userCertificateRepository, UserRepositoryInterface $userRepository)
    {
        $this->userCertificateRepository = $userCertificateRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $userCertificates = $this->userCertificateRepository->all();
        return view('user_certificates.index', compact('userCertificates'));
    }

    public function create()
    {
        $userCertificate = $this->userCertificateRepository->all();
        $users = $this->userRepository->all();
        return view('user_certificates.create', compact('userCertificate', 'users'));
    }

    public function store(UserCertificateRequest $request)
    {
        $userCertificate= $this->userCertificateRepository->all();
        $userCertificate = new UserCertificate();
        $userCertificate->create($request->all());

        return redirect()->route('user_certificates.index')->with('success', 'User Certificate created successfully');
    }

    public function show($id)
    {
        $userCertificate = $this->userCertificateRepository->find($id);
        return $userCertificate;
    }

    public function edit($id)
    {
        $userCertificate = $this->userCertificateRepository->find($id);
        $users = $this->userRepository->all();
        return view('user_certificates.edit', compact('userCertificate', 'users'));;
    }

    public function update(UserCertificateRequest $request, $id)
    {
        $userCertificate = $this->userCertificateRepository->find($id);
        $userCertificate->user_id = $request->user_id;
        $userCertificate->name = $request->name;
        $userCertificate->start_at = $request->start_at;
        $userCertificate->expiry_at = $request->expiry_at;
        $userCertificate->save();

        return redirect()->route('user_certificates.index')->with('success', 'User certificate updated successfully');
    }

    public function destroy($id)
    {
        $this->userCertificateRepository->delete($id);
        return redirect()->route('user_certificates.index')->with('success', 'User certificate deleted successfully');
    }
}
