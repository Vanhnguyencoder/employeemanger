<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SampleMail;

class SendMailController extends Controller
{
    public function index()
    {
        $content = [
            'subject' => 'This is the mail subject',
            'body' => 'Bảng lương tháng'
        ];

        Mail::to('your_email@gmail.com')->send(new SampleMail($content));

        return "Email has been sent.";
    }
}
