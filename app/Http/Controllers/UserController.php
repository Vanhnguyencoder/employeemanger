<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $users = $this->userRepository->all();
        return view('users.index', compact('users'));
    }

    public function create()
    {
        $user = $this->userRepository->all();
        return view('users.create', compact('user'));
    }

    public function store(UserRequest $request)
    {
        $user= $this->userRepository->all();
        $user = new User();
        $user->create($request->all());

        return redirect()->route('users.index')->with('success', 'User  created successfully');
    }

    public function show($id)
    {
        $user = $this->userRepository->find($id);
        return $user;
    }

    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        return view('users.edit', compact('user'));
    }

    public function update(UserRequest $request, $id)
    {
        $user = $this->userRepository->find($id);
        $user->update($request->all());

        return redirect()->route('users.index')->with('success', 'User certificate updated successfully');
    }

    public function destroy($id)
    {
        $user = $this->userRepository->delete($id);
        return redirect()->route('users.index')->with('success', 'User deleted successfully');
    }
}
