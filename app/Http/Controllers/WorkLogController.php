<?php
namespace App\Http\Controllers;

use App\Http\Requests\WorkLogRequest;
use App\Models\WorkLog;
use App\Repositories\WorkLogRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class WorkLogController extends Controller
{
    protected $workLogRepository;
    protected $userRepository;

    public function __construct(WorkLogRepositoryInterface $workLogRepository, UserRepositoryInterface $userRepository)
    {
        $this->workLogRepository = $workLogRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $workLogs = $this->workLogRepository->all();
        return view('work_logs.index', compact('workLogs'));
    }

    public function create()
    {
        $workLog = $this->workLogRepository->all();
        $users = $this->userRepository->all();
        return view('work_logs.create', compact('workLog', 'users'));
    }

    public function store(Request $request)
    {
        $workLog= $this->workLogRepository->all();
        $workLog = new WorkLog();
        $workLog->create($request->all());

        return redirect()->route('work_logs.index')->with('success', 'Work log  created successfully');
    }

    public function show($id)
    {
        $workLog = $this->workLogRepository->find($id);
        return $workLog;
    }

    public function edit($id)
    {
        $workLog = $this->workLogRepository->find($id);
        $users = $this->userRepository->all();
        return view('work_logs.edit', compact('workLog', 'users'));
    }

    public function update(WorkLogRequest $request, $id)
    {
        $workLog = $this->workLogRepository->find($id);
        $workLog->user_id = $request->user_id;
        $workLog->work_date = $request->work_date;
        $workLog->bonus = $request->bonus;
        $workLog->penalty = $request->penalty;
        $workLog->save();

        return redirect()->route('work_logs.index')->with('success', 'Work log update successfully');
    }

    public function destroy($id)
    {
        $this->workLogRepository->delete($id);
        return redirect()->route('work_logs.index')->with('success', 'Work log deleted successfully');
    }
}
