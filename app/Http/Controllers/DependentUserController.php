<?php
namespace App\Http\Controllers;

use App\Http\Requests\DependentUserRequest;
use App\Models\DependentUser;
use App\Repositories\DependentUserRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class DependentUserController extends Controller
{
    protected $dependentUserRepository;
    protected $userRepository;

    public function __construct(DependentUserRepositoryInterface $dependentUserRepository, UserRepositoryInterface $userRepository)
    {
        $this->dependentUserRepository = $dependentUserRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $dependentUsers = $this->dependentUserRepository->all();

        return view('dependent_users.index', compact('dependentUsers'));
    }

    public function create()
    {
        $dependentUser = $this->dependentUserRepository->all();
        $users = $this->userRepository->all();
        return view('dependent_users.create', compact('dependentUser', 'users'));
    }

    public function store(DependentUserRequest $request)
    {
        $dependentUser= $this->dependentUserRepository->all();
        $dependentUser = new DependentUser();
        $dependentUser->create($request->all());

        return redirect()->route('dependent_users.index')->with('success', 'Dependent user  created successfully');
    }

    public function show($id)
    {
        $dependentUser = $this->dependentUserRepository->find($id);
        return $dependentUser;
    }

    public function edit($id)
    {
        $dependentUser = $this->dependentUserRepository->find($id);
        $users = $this->userRepository->all();
        return view('dependent_users.edit', compact('dependentUser', 'users'));
    }

    public function update(DependentUserRequest $request, $id)
    {
        $dependentUser = $this->dependentUserRepository->find($id);
        $dependentUser->update($request->all());

        return redirect()->route('dependent_users.index')->with('success', 'Dependent user updated successfully');
    }

    public function destroy($id)
    {
        $this->dependentUserRepository->delete($id);
        return redirect()->route('dependent_users.index')->with('success', 'Dependent user deleted successfully');
    }
}
