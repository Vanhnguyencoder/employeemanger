<?php

use App\Http\Controllers\DependentUserController;
use App\Http\Controllers\SendMailController;
use App\Http\Controllers\UserCertificateController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WorkLogController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//USER ROUTES 
Route::get('/user', [UserController::class, 'index'])->name('users.index');
Route::get('/user/create', [UserController::class, 'create'])->name('users.create');
Route::post('/user', [UserController::class, 'store'])->name('users.store');
// Route::get('/user/{id}', [UserController::class, 'show'])->name('users.show');
Route::get('/user/{id}/edit', [UserController::class, 'edit'])->name('users.edit');
Route::put('/user/{id}', [UserController::class, 'update'])->name('users.update');
Route::delete('/user/{id}', [UserController::class, 'destroy'])->name('users.delete');

// USER CERTIFICATE ROUTES 
Route::get('/user_certificate', [UserCertificateController::class, 'index'])->name('user_certificates.index');
Route::get('/user_certificate/create', [UserCertificateController::class, 'create'])->name('user_certificates.create');
Route::post('/user_certificate', [UserCertificateController::class, 'store'])->name('user_certificates.store');
// Route::get('/user_certificate/{id}', [UserCertificateController::class, 'show'])->name('user_certificates.show');
Route::get('/user_certificate/{id}/edit', [UserCertificateController::class, 'edit'])->name('user_certificates.edit');
Route::put('/user_certificate/{id}', [UserCertificateController::class, 'update'])->name('user_certificates.update');
Route::delete('/user_certificate/{id}', [UserCertificateController::class, 'destroy'])->name('user_certificates.delete');

//DEPENDENT USER ROUTES 
Route::get('/dependent_user', [DependentUserController::class, 'index'])->name('dependent_users.index');
Route::get('/dependent_user/create', [DependentUserController::class, 'create'])->name('dependent_users.create');
Route::post('/dependent_user', [DependentUserController::class, 'store'])->name('dependent_users.store');
// Route::get('/dependent_user/{id}', [DependentUserController::class, 'show'])->name('dependent_users.show');
Route::get('/dependent_user/{id}/edit', [DependentUserController::class, 'edit'])->name('dependent_users.edit');
Route::put('/dependent_user/{id}', [DependentUserController::class, 'update'])->name('dependent_users.update');
Route::delete('/dependent_user/{id}', [DependentUserController::class, 'destroy'])->name('dependent_users.delete');

//WORK LOG ROUTES 
Route::get('/work_log', [WorkLogController::class, 'index'])->name('work_logs.index');
Route::get('/work_log/create', [WorkLogController::class, 'create'])->name('work_logs.create');
Route::post('/work_log', [WorkLogController::class, 'store'])->name('work_logs.store');
// Route::get('/work_log/{id}', [WorkLogController::class, 'show'])->name('work_logs.show');
Route::get('/work_log/{id}/edit', [WorkLogController::class, 'edit'])->name('work_logs.edit');
Route::put('/work_log/{id}', [WorkLogController::class, 'update'])->name('work_logs.update');
Route::delete('/work_log/{id}', [WorkLogController::class, 'destroy'])->name('work_logs.delete');


// MAIL ROUTES
Route::get('/send-mail', [SendMailController::class, 'index']);
